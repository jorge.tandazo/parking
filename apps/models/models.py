from django import forms
from django.db import models
# Create your models here.
# Parking,Square,Ticket,Account,User,Role,Car


class Parking(models.Model):
    # Se declara un id del cliente como clave primaria
    parkingId = models.AutoField(primary_key=True)
    name = models.CharField(max_length=10, unique=True, null=False)
    location = models.TextField(max_length=50, default='sin dirección')
    # auto_now fecha del sistema, auto_now_add= false permite modificar la fecha
    email = models.EmailField(max_length=50, null=False)
    phone = models.CharField(max_length=13)
    # celular=models.CharField(max_length=13)


class Square(models.Model):
    statusList = (
        ('ocupado', 'Ocupado'),
        ('libre', 'Libre')
    )
    squareId = models.AutoField(primary_key=True)
    nameSquare = models.CharField(max_length=10, null=False)
    statusSquare = models.CharField(
        max_length=20, choices=statusList, default='libre', null=False)

    Parking = models.ForeignKey(
        'parking',
        on_delete=models.CASCADE,
    )


class Ticket(models.Model):
    ticketId = models.AutoField(primary_key=True)
    startTime = models.TimeField(null=True, blank=True)
    finishTime = models.TimeField(null=True, blank=True)
    cost = models.DecimalField(null=False, max_digits=6, decimal_places=2)
    startDate = models.DateField(null=True, blank=True)
    finishDate = models.DateField(null=True, blank=True)

class Account(models.Model):
    statusList = (
        ('activo', 'Activo'),
        ('inactivo', 'Inactivo')
    )
    accountId = models.AutoField(primary_key=True)
    email = models.EmailField(null=False,max_length = 25)
    password = models.CharField(null=False,max_length=20)
    statusAccout = models.CharField(
        max_length=20, choices=statusList, default='activo', null=False)

class Car(models.Model):
    cartId = models.AutoField(primary_key=True)
    vehicleRegistration = models.CharField(null=False,max_length=20)
    brand = models.CharField(null=False,max_length=20)
    color = models.CharField(null=False,max_length=20)

class User(models.Model):
    userId = models.AutoField(primary_key=True)
    name = models.CharField(null=False,max_length=20)
    lastName = models.CharField(null=False,max_length=20)
    age = models.IntegerField(null=False,max_length=3)
    phone =  models.IntegerField(null=False,max_length=10)

class Role(models.Model):
    statusList = (
        ('gerente', 'Gerente'),
        ('usuario', 'Usuario')
    )
    roleId = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, choices=statusList, default='usuario', null=False)
   

