from django.contrib import admin

# Register your models here.
from .models import Parking
#crea un administrador utilizando herencia ModelAdmin
class adminParking(admin.ModelAdmin):
    list_display=["name","location","email","phone"]
    #list_editable=["name","location","email","phone"]
    #list_filter=["genero","estadoCivil"]
    search_fields=["name","location","email","phone"]
    class Meta:
        model=Parking
admin.site.register(Parking, adminParking)

from .models import Square
class adminSquare(admin.ModelAdmin):
    #list_display=["nameSquare","statusSquare"]
    #list_editable=["nameSquare","statusSquare"]
    #list_filter=["statusSquare"]
    #search_fields=["nameSquare","statusSquare"]
    class Meta:
        model=Square
admin.site.register(Square, adminSquare)
from .models import Ticket
class adminTicket(admin.ModelAdmin):
    
    list_display=["startTime","finishTime","cost","startDate","finishDate"]
    #list_editable=["nameSquare","statusSquare"]
    #list_filter=["statusSquare"]
    #search_fields=["nameSquare","statusSquare"]
    class Meta:
        model=Ticket
admin.site.register(Ticket, adminTicket)

from .models import Account
class adminAccount(admin.ModelAdmin):
    list_display=["email","password","statusAccout"]
    class Meta:
        model=Account
admin.site.register(Account,adminAccount)

from .models import Car
class adminCar(admin.ModelAdmin):
    list_display=["vehicleRegistration","brand","color"]
    class Meta:
        model=Car
admin.site.register(Car,adminCar)

from .models import User
class adminUser(admin.ModelAdmin):
    list_display=["name","lastName","age","phone"]
    class Meta:
        model=User
admin.site.register(User,adminUser)

from .models import Role
class adminRole(admin.ModelAdmin):
    list_display=["name"]
    class Meta:
        model=Role
admin.site.register(Role,adminRole)